import React, { useState, useEffect } from "react";
import useNumber from './utils/useNumber';
const CounterHook = () => {
    const[number, setCount] = useState(0);
    const[age, setAge] = useState(18);
    
    useNumber(number)

    return(
        <div>
            <h1>{number}</h1>
            <button
                onClick={()=>{
                    setCount( number + 1 )
                }}
            >Dodaj</button>

            <h1>{age}</h1>
            <button
                onClick={()=>{
                    setAge( age + 1 )
                }}
            >Dodaj</button>
        </div>
    );
}
export default CounterHook;