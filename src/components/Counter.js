import React, { Component } from "react";

class Counter extends Component {
    constructor(){
        super();
        this.state = {
            number: 0
        }
    }
    render(){
        return (
            <div>
                <h1>{this.state.number}</h1>
                <button
                    onClick={()=>{
                        this.setState({ number : this.state.number + 1 })
                    }}
                >Dodaj</button>
            </div>
        );
    }
}
export default Counter;