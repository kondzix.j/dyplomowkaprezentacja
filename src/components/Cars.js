import React, { Component } from "react";
import ReactDOM from 'react-dom';

class Cars extends Component{
    constructor(){
        super();
        this.state = {
            name : "Volvo"
        }
    }
    render(){
        return <h1>{this.state.name}</h1>
    }
}

export default Cars;



