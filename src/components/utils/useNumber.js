import { useEffect } from 'react';
const useNumber = (number) => {
    useEffect(() => {
        if(number < 30){
            document.title = `Naciśnięto ${number} razy`;
        } else {
            document.title = `Za duzo!`;
        }
        
    });
}
export default useNumber