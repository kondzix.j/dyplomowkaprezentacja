import './App.css';
import React, { Component } from 'react';
import Cars from './components/Cars'
import Counter from './components/Counter'
import CounterHook from './components/CounterHook';
class App extends Component{
  render() {
    return (
      <div className="App">
        <Cars name="Opel"/>
        <Counter></Counter>
        <CounterHook></CounterHook>
      </div>
    )
  }
}

export default App;
